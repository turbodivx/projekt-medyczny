import numpy as np

def getNewGeneration(genalger, evaluation):

    if genalger.selection_method['method'] == 'RW':
        getNewGeneration_RouletteWheel(genalger, evaluation)  # selection
    elif genalger.selection_method['method'] == 'SUS':
        getNewGeneration_StochasticUniversalSampling(genalger, evaluation)  # selection
    elif genalger.selection_method['method'] == 'RS':
        getNewGeneration_RankSelection(genalger, evaluation, genalger.selection_method['param1'])  # selection
    elif genalger.selection_method['method'] == 'T':
        getNewGeneration_Tournament(genalger, evaluation, genalger.selection_method['param1'], genalger.selection_method['param2'])  # selection


def getNewGeneration_RouletteWheel(genalger, M_eval):
    """Perform selection according to the roulette wheel strategy."""

    roulette_stripe = np.cumsum(M_eval) / M_eval.sum()

    roulette_turns = np.random.rand(genalger.population_size)

    rep_chromosomes_ids = np.searchsorted(roulette_stripe, roulette_turns, side='right')
    genalger.chromosomes = genalger.chromosomes[:, rep_chromosomes_ids]
    genalger.generation_number = genalger.generation_number + 1

def getNewGeneration_StochasticUniversalSampling(genalger, M_eval):
    """Perform selection according to the Stochastic Universal Sampling method.
    Do one wheel spin as in Roulette Wheel selection and select all
    chromosomes at once by taking small (1/*population_size) steps for
    *population_size*."""

    roulette_stripe = np.cumsum(M_eval) / M_eval.sum()
    roulette_turn = np.random.rand(1)
    steps = np.mod(roulette_turn + np.arange(0, 1, 1 / float(genalger.population_size)), 1)
    rep_chromosomes_ids = np.searchsorted(roulette_stripe, np.array(steps), side='right')

    genalger.chromosomes = genalger.chromosomes[:, rep_chromosomes_ids]
    genalger.generation_number = genalger.generation_number + 1

def getNewGeneration_RankSelection(genalger, M_eval, favouritism):
    """Perform selection according to the Rank selection.
    Choose unique chromosomes, rank them according to fittness
    and do Roulette Wheel selection for these ranks.
    In order to prioritize highly ranked chromosomes increase
    favouritism value. """

    # Leave only unique
    unique_chromosomes, unique_ids = np.unique(genalger.chromosomes, return_index=True, axis=1)
    unique_M_eval = np.array(M_eval).flatten()[unique_ids]

    # Rank order based on M_eval
    order = unique_M_eval.argsort()[:-1]
    unique_chromosomes = unique_chromosomes[:, order[:]]

    # Create ranks
    ranks = np.power(range(1, (unique_chromosomes.shape[1]) + 1), favouritism)
    roulette_turns = np.random.rand(genalger.population_size)

    roulette_stripe = np.cumsum(ranks) / ranks.sum()
    rep_chromosomes_ids = np.searchsorted(roulette_stripe, roulette_turns, side='right')

    genalger.chromosomes = unique_chromosomes[:, rep_chromosomes_ids]
    genalger.generation_number = genalger.generation_number + 1


def getNewGeneration_Tournament(genalger, M_eval, winners, tournament_size):
    """Perform selection according to the Torunament rules.
    Select *turnament_size* number of participants for tournament
    and choose the most fitting one.

    chromosomes - list of individuals
    winners - amount of accepted chromosomes from tournament
    tournament size - number of individuals participating in tournament"""

    new_chromosomes = []
    M_eval_flat = np.array(M_eval).flatten()

    for i in range(int(genalger.population_size / winners)):
        participants = np.random.choice(genalger.population_size, tournament_size)
        new_chromosomes.extend([x[0] for x in sorted([(x, M_eval_flat[x]) for x in participants],
                                                     key=lambda x: x[1])][:winners])

    if (genalger.population_size % winners) != 0:
        new_chromosomes.extend(np.random.choice
                               (genalger.population_size, genalger.population_size % winners))

    genalger.chromosomes = genalger.chromosomes[:, new_chromosomes]
    genalger.generation_number = genalger.generation_number + 1