# -*- coding: utf-8 -*-
"""
Created on Sat Apr 07 14:12:00 2018

@author: mysh9
"""
import numpy as np


def getZeroGeneration(genalger):
    """Generate the zero generation of chromosomes."""
        
    chromosomes = np.zeros((genalger.chromosome_length,
                            genalger.population_size), dtype=int)
    
    for col in range(genalger.population_size):
        chosen_indices = np.sort(np.random.choice(genalger.chromosome_length,
                                 genalger.init_ones, replace=False))
        chromosomes[chosen_indices, col] = 1
    
    genalger.chromosomes = chromosomes


def evaluateGeneration(genalger):
    
    # add penalty for higher number of normalisers
    # try different options
    chsum = genalger.chromosomes.sum(axis=0)
    if genalger.penalty_type == 'none':
        penalty_function = np.ones(genalger.chromosomes.shape[1])
    elif genalger.penalty_type == 'exp':
        penalty_function = np.exp(-chsum)
    elif genalger.penalty_type == 'power':
        penalty_function = np.power(abs(chsum-genalger.penalty_target),
                                    genalger.penalty_param[genalger.generation_number]) + 0.1
    elif genalger.penalty_type == 'U-shape':
        penalty_function = genalger.penalty_param[
                genalger.generation_number] * np.power((chsum-genalger.penalty_target), 2) + 1
    
    M_penalised = genalger.current_Ms*penalty_function
    
    # penalty introduced by nulls in the original data
    nulls_penalty = np.matmul(genalger.null_stats.reshape(1,-1), genalger.chromosomes)
    M_penalised = M_penalised * np.exp(genalger.null_stats_contribution*nulls_penalty)
    
    M_penalised[np.isnan(M_penalised)] = 2**127 # a high value to exclude
    # the normaliser from selection; introduced here, not in current_Ms, so as
    # not to blind the Ms std statistics
    
    # transform the penalised M values using the chosen evaluation function
    if genalger.ev_type == 'plain':
        M_eval = np.power(M_penalised, -1.)
    elif genalger.ev_type == 'power':
        M_eval = np.power(M_penalised, -genalger.ev_param[genalger.generation_number])
    elif genalger.ev_type == 'exp':
        M_eval = np.exp(-np.array(M_penalised))
    
    
    return M_eval


def mutation(genalger):
    """Perform mutation for n chromosomes/loci if the drawn random number
    (float, [0,1)) is lower than prob_th."""
    
    i = genalger.generation_number - 1
    # first mutation is performed on generation 1, not 0
    
    n_to_perform = np.sum(np.random.rand(genalger.n_mutations[i]) <= genalger.p_mutation[i])
    chromosome_ids = np.random.randint(genalger.chromosomes.shape[1], size=n_to_perform)
    loci = np.random.randint(genalger.chromosomes.shape[0], size=n_to_perform)
    genalger.chromosomes[loci, chromosome_ids] = 1 - genalger.chromosomes[loci, chromosome_ids]
    

def crossing_over(genalger):
    """Perform crossing-over on the population with (upper) threshold
    probability of prob_th."""
    
    chromosome_id_pairs = np.arange(genalger.chromosomes.shape[1])
    np.random.shuffle(chromosome_id_pairs)
    if chromosome_id_pairs.size%2 != 0:
        chromosome_id_pairs = chromosome_id_pairs[:-1] # assert even number
    chromosome_id_pairs = chromosome_id_pairs.reshape(-1,2)    

    chromosomes_buffer = np.copy(genalger.chromosomes)
    
    ids = xo_generate_ids(n_chrom=genalger.chromosomes.shape[0],
                          n_pairs=chromosome_id_pairs.shape[0],
                          xo_type=genalger.xo_type)
    
    crossing_rand_probs = np.random.rand(chromosome_id_pairs.shape[0])
    for p in np.where(crossing_rand_probs <= genalger.p_crossing_over[genalger.generation_number-1])[0]:
        chpair = chromosome_id_pairs[p,:]
        genalger.chromosomes[ids[p,:],chpair[1]] = chromosomes_buffer[ids[p,:],chpair[0]]
        genalger.chromosomes[ids[p,:],chpair[0]] = chromosomes_buffer[ids[p,:],chpair[1]]
    
    
def xo_generate_ids(n_chrom, n_pairs, xo_type='decoupled'):
    """Helper function fo crossing_over()."""
    if xo_type == 'one-point':
        loci = np.random.randint(n_chrom, size=n_pairs)
        lloci = np.stack(n_chrom*[loci]).transpose()
        ids_prepare = np.stack(n_pairs*[np.arange(n_chrom)])
        ids = np.where(ids_prepare>lloci, True, False)    
    
    elif xo_type == 'two-point':
        loci = np.random.randint(n_chrom, size=(2, n_pairs))
        loci.sort(axis=0)
        lloci1 = np.stack(n_chrom*[loci[0,:]]).transpose()
        lloci2 = np.stack(n_chrom*[loci[1,:]]).transpose()
        ids_prepare = np.stack(n_pairs*[np.arange(n_chrom)])
        ids = np.where((ids_prepare>lloci1) & (ids_prepare<=lloci2), True, False)
    
    elif xo_type == 'decoupled':
        ids = np.random.randint(2, size=(n_pairs, n_chrom)).astype(bool)
        
    else:
        print("Invalid crossing-over type")
        ids = np.zeros((n_pairs, n_chrom), dtype=bool)
        
    return ids

    