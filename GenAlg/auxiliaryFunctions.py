# -*- coding: utf-8 -*-
import os
import pandas as pd
import numpy as np
import time
import imp

MMpath = open("MetaMirs_path.txt").read() # file with absolute path to the MetaMirs directory
unify_nomenclature = imp.load_source('unify_nomenclature', MMpath+'\\unify_nomenclature.py')

def loadData(filenumber=0, whereFrom='Expr'):
    """Load data in Pandas DataFrame format.
    Arguments:
        - filenumber (int) - which file from the directory to use;
        - whereFrom (String) - specify the directory as one of the following:
            - 'Expr' (default) - the 'Expr' folder with .xls files;
            - 'here' - the lone file in the code directory;
            - path to the data folder."""
    
    if whereFrom == 'Expr':
        data_dir = os.path.join(MMpath, 'Expr')
        data_fnames = os.listdir(data_dir)
        data_fname = os.path.join(data_dir, data_fnames[filenumber]) # give a number: 0-10
    elif whereFrom == 'here':
        data_fname = 'GSE67075.xlsx'
    else:
        data_fnames = os.listdir(whereFrom)
        data_fname = os.path.join(data_dir, data_fnames[filenumber])
    
    print ("Data from file: %s" %data_fname)
    data_pd = pd.read_excel(data_fname, index_col=0, header=0)
    group_indicator = get_group_indicator(os.path.split(data_fname)[-1])
    return data_pd, group_indicator


def optimisedDropNa(data_pd, k=0.9, verbose=False):
    """Remove rows/cols containing null cells to retain maximal amount of data.
    Arguments:
        - data_pd - the data in Pandas Dataframe format;
        - k - float in range (0,1]; in given cycle, remove all rows/cols
            containing no less than k*maximal_no_of_nulls_in_a_col_or_row;
        - verbose - boolean; when True, print report of the null removal."""
    
    # first, remove duplicates
    data_pd = unify_nomenclature.remove_duplicates(data_pd)
    
    init_no_samples = data_pd.shape[1]
    
    null_stats = data_pd.isnull().sum(axis=1)
    
    if verbose: print("Initial data shape: %d rows, %d cols" %data_pd.shape);
    tstart = time.time()
    cycles = 0
    while(data_pd.isnull().sum().sum()>0):
        if verbose: print("Number of nulls: %d;" %data_pd.isnull().sum().sum()),
        
        nullsByCol = data_pd.isnull().sum(axis=0)/data_pd.shape[0]
        nullsByRow = data_pd.isnull().sum(axis=1)/data_pd.shape[1]
        
        if nullsByRow.max() >= nullsByCol.max():
            rows_to_remove = data_pd.index[np.where(nullsByRow >= k*nullsByRow.max())[0]]
            if verbose: print("removing %d row(s)" %len(rows_to_remove))
            data_pd.drop(rows_to_remove, axis=0, inplace=True)
        else:
            cols_to_remove = data_pd.columns[np.where(nullsByCol >= k*nullsByCol.max())[0]]
            if verbose: print("removing %d col(s)" %len(cols_to_remove))
            data_pd.drop(cols_to_remove, axis=1, inplace=True)
        cycles = cycles + 1;
    
    null_stats = null_stats[data_pd.index]/init_no_samples # normalise null stats
        
    if verbose:
        print("Nulls removed in %.2f s (%d cycle(s))" %((time.time()-tstart), cycles))
        print("Final data shape: %d rows, %d cols\n" %data_pd.shape);
        
    return data_pd, null_stats.values

def define_group_indicators():
    # according to MetaMirs/toolbox.py
    group_indicator = dict()
    group_indicator['1_GSE47125.xls'] = get_gi_list([7,8])
    group_indicator['2_GSE49823.xls'] = get_gi_list([13,13])
    group_indicator['3_GSE50013.xls'] = 20*[1, 2]
    group_indicator['4_GSE57661.xls'] = get_gi_list([24, 24])
    group_indicator['5_GSE59520.xls'] = get_gi_list([8, 10, 15, 3])
    group_indicator['6_GSE67075.xls'] = get_gi_list(6*[8])
    group_indicator['7_GSE68314.xls'] = get_gi_list(2*[4])
    group_indicator['8_GSE75389.xls'] = get_gi_list(2*[3])
    group_indicator['90_GSE75391.xls'] = get_gi_list(2*[7])
    group_indicator['91_GSE85903.xls'] = get_gi_list([3, 5, 5])
    group_indicator['92_GSE90828.xls'] = get_gi_list([30, 23])
    return group_indicator

def get_gi_list(q):
    gi_list = []
    for g, n in enumerate(q):
        gi_list.extend(n*[g])
    return gi_list

def get_group_indicator(filename):
    gi_dict = define_group_indicators()
    if filename in gi_dict.keys():
        return gi_dict[filename]
    else:
        return None