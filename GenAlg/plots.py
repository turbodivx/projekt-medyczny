# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

def genAlgCycles(genalger):
    """Plot the genetic algorithm statistics throughout the generations.
    Arguments:
        - best_Ms - list of best M values;
        - Ms_std - list of the standard deviations;
        - best_ch_sum - list of best chromosome sums
    (see main.py for reference)."""
    
    best_Ms = np.nanmin(genalger.all_Ms, axis=0)
    Ms_std = np.nanstd(genalger.all_Ms, axis=0)
    best_ch_sum = genalger.best_chrom.sum(axis=0)
    
    plt.figure(figsize=(8,6))
    ax1 = plt.subplot(311)
    plt.plot(np.arange(len(best_Ms)), np.array(best_Ms))
    plt.grid(color='#dedede')
    plt.xlabel('Generations')
    plt.ylabel('Stability measure (M)')
    plt.title('Best stability measure in consecutive generations')
    
    plt.subplot(312, sharex=ax1)
    plt.plot(np.arange(len(Ms_std)), np.array(Ms_std))
    plt.grid(color='#dedede')
    plt.xlabel('Generations')
    plt.ylabel('M Standard deviation')
    plt.title('Standard deviation of the stability measure in consecutive generations')
    
    plt.subplot(313, sharex=ax1)
    plt.plot(np.arange(len(best_ch_sum)), np.array(best_ch_sum))
    plt.grid(color='#dedede')
    plt.xlabel('Generations')
    plt.ylabel('Chromosome sum')
    plt.title('Best number of normalisers')
    plt.tight_layout()
    plt.show()

def chromAccum(chromosomes_accumulation,
               title="Accumulated chromosomes in consecutive generations"):
    """Plot summed chromosomes throughout the generations to depict how the
    chosen normalisers emerge in consecutive cycles.
    Argument: chromosomes_accumulation - 2D np array of shape: (loci, generations)."""
    
    plt.figure()
    plt.imshow(chromosomes_accumulation, cmap='winter')
    plt.title(title)
    plt.xlabel("Generations")
    plt.ylabel("Locus")
    plt.colorbar()
    plt.show()

def chromosomesSubplots(genalger, subplot, title="Chromosomes"):
    """Plot the current generation of chromosomes in a subplot.
    Arguments:
        - chromosomes - 2D np array of shape: [loci, chromosome_id];
        - subplot - subplot number (e.g. 211);
        - title - subplot title (String)."""
        
    plt.figure('Chromosomes')
    plt.subplot(subplot)
    plt.imshow(genalger.chromosomes, cmap='winter')
    plt.title(title)
    plt.ylabel("Locus")
    plt.xlabel("Chromosome id")
    plt.tight_layout()
    plt.show()

def paramsPlots(genalger,
                paramslist = ['p_mutation', 'n_mutations', 'p_crossing_over',
                         'ev_param', 'penalty_param'],
                title="Gen cycles parameters in consecutive generations"):
    plt.figure("Params")
    
    n = len(paramslist)
    for i, param in enumerate(paramslist):
        plt.subplot((n+1)//2, 2, i+1)
        plt.plot(getattr(genalger, param))
        plt.title(param)
        plt.grid()
    plt.xlabel("Generations")
    plt.suptitle(title)
    
    plt.tight_layout()
    plt.subplots_adjust(top=0.88)
    plt.show()