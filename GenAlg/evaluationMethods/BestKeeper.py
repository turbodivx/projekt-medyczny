# -*- coding: utf-8 -*-

import numpy as np
from scipy.stats.mstats import gmean
from scipy.stats import spearmanr, kendalltau

class BestKeeper:
    def __init__(self, data, corr_method='spearmanr'):
        self.model = "BestKeeper"
        self.data_log = np.log2(data)
        self.corr_method = corr_method
        
    
    def get_stability(self, chromosome):
        data_normalisers_log = self.data_log[chromosome==1,:]
        data_rest_log = self.data_log[chromosome==0,:]
    
        BKI = gmean(data_normalisers_log, axis=0) # best keeper index
        corr = self.get_correlation(BKI, data_rest_log)        
        return np.mean(corr) # <--- is that fine as the stability measure?
    
    
    def get_correlation(self, BKI, data_rest_log):
        if self.corr_method in ['pearsonr', 'pearson']:
            corr = np.corrcoef(BKI.reshape(1,-1),data_rest_log)[1:,0]
        elif self.corr_method in ['spearmanr', 'spearman']:
            corr = spearmanr(BKI.reshape(1,-1),data_rest_log, axis=1)[0][1:,0]
        elif self.corr_method == 'kendalltau':
            corr = [kendalltau(data_rest_log[i,:], BKI)[0] for i in range(data_rest_log.shape[0])]
        return corr
        
#------------------------------------------------------------------------------
if __name__ == '__main__':
    import auxiliaryFunctions as aux
    data_pd, group_indicator = aux.loadData(filenumber=0, whereFrom='here')
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    BK = BestKeeper(data_pd.values, corr_method='pearsonr')
    M = BK.get_stability(np.random.randint(2,size=data_pd.shape[0]))
    print (M)