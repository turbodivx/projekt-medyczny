# -*- coding: utf-8 -*-
import numpy as np
import auxiliaryFunctions as aux

class NormFinder:
    def __init__(self, data, group_indicator):
        self.model = "NormFinder"
        self.data_log = np.log(data)
        self.n_genes, self.n_samples = data.shape
        self.est_mean = None
        self.est_var = None
        
        self.set_groups(group_indicator)
        self.compute_params()
    
    def set_groups(self, group_indicator):
        if group_indicator is None:
            self.groups = [range(self.n_samples)] # all samples belong to a single group
        else:
            self.groups = [[i for i, v in enumerate(group_indicator) if v==k] for k in set(group_indicator)]
        self.G = len(self.groups)
        
    
    def compute_params(self):
        #--------------------------------------------------------------------------
        # Intragroup variability
        # target: sigma_ig2 (square) - variance for gene i and group g
        
        # residuals - terms independent of gene and sample levels
        r_igj = self.data_log - np.mean(self.data_log, axis=1).reshape(-1,1) - np.mean(self.data_log, axis=0).reshape(1,-1) + np.mean(self.data_log)
        r_igj2 = r_igj ** 2
        
        # sample variances
        s_ig2 = np.zeros((self.data_log.shape[0], self.G))
        
        for i, group in enumerate(self.groups):
            s_ig2[:,i] = np.sum(r_igj2[:,group], axis=1)/((len(group)-1)*(1-2./self.n_genes))
        
        # estimate of group variance
        sigma_ig2 = s_ig2 - (1./(self.n_genes*(self.n_genes-1)))*np.sum(s_ig2, axis=0).reshape(1,-1)
        
        
        #--------------------------------------------------------------------------
        # Inter-group variability
        # difference in expression level across the groups
        # delta_ig, estimated by d_ig
        # (generic code, works for G=1 too)
        
        # z_ig: the average of the measured gene expressions for gene i in group g
        z_ig = np.zeros((self.data_log.shape[0], self.G))
        for i, group in enumerate(self.groups):
            z_ig[:,i] = np.mean(self.data_log[:,group], axis=1)
        d_ig = z_ig - np.mean(z_ig, axis=1).reshape(-1,1) - np.mean(z_ig, axis=0).reshape(1,-1) + np.mean(z_ig)
        
        
        #--------------------------------------------------------------------------
        # sigma_ig/n_g = variance of z_ig - used multiple times later
        z_var = sigma_ig2/np.array([len(group) for group in self.groups])
        
        # estmate of gamma2 - variance of a_ig
        gamma2 = 1./((self.n_genes-1)*self.G-1) * np.sum(d_ig**2) - 1./(self.n_genes*self.G)*np.sum(z_var)
        gamma2 = max(gamma2, 0)
        
        #--------------------------------------------------------------------------
        # value of interest: est = z_ig - phi_g - a_i
        # - distribution of a_ig - a_i
        # est_mean, est_var - estimated mean and variance of est
        
        self.est_mean = gamma2*abs(d_ig)/(gamma2 + z_var)
        self.est_var = z_var + (gamma2*z_var)/(gamma2 + z_var)
        

    def get_stability(self, A=None, chromosome=None):
        """A - set of indices of the housekeeping genes to be 'synthesised' by average
        while calculating the stability measure (rho)
        OR
        chromosome - with 1 meaning 'normaliser' and 0 - 'not normaliser'"""
        if A is None:
            A = np.where(chromosome==1)[0]
        
        a = len(A)
        c = np.sqrt(self.n_genes/(self.n_genes-a)) # correction factor
        
        rho_Ag = ((c*np.sum(self.est_mean[A,:], axis=0)) + np.sqrt(np.sum(self.est_var[A,:], axis=0))) / a
        rho_A = np.sum(rho_Ag) / a
        # ^ I'd say it should rather be 1./G * np.sum(rho_Ag), i.e. np.mean(rho_Ag)
        return rho_A


#------------------------------------------------------------------------------
    
if __name__ == '__main__':
    data_pd, group_indicator = aux.loadData(filenumber=0, whereFrom='here')
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    NF = NormFinder(data_pd.values, group_indicator)
    rho = NF.get_stability(A=[1,2,3])
    rho2 = NF.get_stability(chromosome=np.random.randint(2,size=data_pd.shape[0]))
    print (rho, rho2)