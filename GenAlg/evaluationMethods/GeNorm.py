# -*- coding: utf-8 -*-

import numpy as np
from scipy.stats.mstats import gmean, hmean

class GeNorm:
    def __init__(self, data, synth_method='arithmetic'):
        self.model = "GeNorm"
        self.data = data
        self.get_synthesiser(synth_method)
        
    
    def get_stability(self, chromosome):
        data_normalisers = self.data[chromosome==1,:]
        data_rest = self.data[chromosome==0,:]
        
        synth_normaliser = self.synth(data_normalisers, axis=0)      
        data_logratios = np.log2((data_rest/synth_normaliser))
        data_row_stdevs = np.std(data_logratios, axis=1)
        return np.mean(data_row_stdevs) # stability measure
    
    
    def get_synthesiser(self, synth_method):
        if synth_method == 'arithmetic':
            self.synth = np.average
        elif synth_method == 'geometric':
            self.synth = gmean
        elif synth_method == 'harmonic':
            self.synth = hmean
        else:
            self.synth = synth_method
            
#------------------------------------------------------------------------------    
if __name__ == '__main__':
    import auxiliaryFunctions as aux
    data_pd, group_indicator = aux.loadData(filenumber=0, whereFrom='here')
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    GN = GeNorm(data_pd.values, synth_method='geometric')
    M = GN.get_stability(np.random.randint(2,size=data_pd.shape[0]))
    print (M)