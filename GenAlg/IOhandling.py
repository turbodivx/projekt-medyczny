import pickle
import datetime

from GenAlg.GenAlger import GenAlger

def saveToFile(genalger):
    result_to_be_saved = {
        "population_size": genalger.population_size,
        "n_generations": genalger.generation_number,
        "estimator": genalger.estimator,
        "ev_param": genalger.ev_param_dict,
        "xo_type": genalger.xo_type,
        "null_stats": genalger.null_stats,
        "null_stats_contribution": genalger.null_stats_contribution,

        "penalty_param": genalger.penalty_param,
        "penalty_type": genalger.penalty_type,
        "penalty_target": genalger.penalty_target,

        "selection_method": genalger.selection_method,
        "p_mutation": genalger.p_mutation_dict,
        "n_mutation_dict": genalger.n_mutation_dict,
        "p_crossing_over": genalger.p_crossing_over_dict,

        "all_Ms": genalger.all_Ms,
        "chromosomes_accumulation": genalger.chromosomes_accumulation,
        "best_chrom": genalger.best_chrom,
        "timestamp": datetime.datetime.now().date(),
        "data_filename": str(genalger.estimator.model) + str(genalger.penalty_type) + str(genalger.selection_method['method'])
                         + str(datetime.datetime.now().date())
    }

    with open(result_to_be_saved['data_filename'], 'wb') as file:
        pickle.dump(result_to_be_saved, file)

def openGenalgerFile(fname):

    f = open(fname, "rb")
    x = pickle.load(f)
    f.close()
    return x

# estimators
genalger = GenAlger()
genalger = openGenalgerFile('GeNormU-shapeRW2018-06-17')
print(genalger)
