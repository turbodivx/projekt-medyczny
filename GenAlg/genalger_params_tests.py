# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from time import time

import auxiliaryFunctions as aux
from evaluationMethods.NormFinder import NormFinder
from evaluationMethods.GeNorm import GeNorm
from evaluationMethods.BestKeeper import BestKeeper
from GenAlger import GenAlger
from IOhandling import saveToFile


def param1():
    data_pd, group_indicator = aux.loadData(filenumber=0, whereFrom='here')
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    
    GN = GeNorm(data_pd.values)
    genalger = GenAlger(estimator=GN)
    genalger.loadData(data_pd, null_stats=null_stats)

    
    k = 3 # number of generations
    genalger.ev_param_dict = {'method': 'linspace', 'params': {'start': 2.5, 'stop': 3., 'num': k}}
    genalger.penalty_param_dict = {'method': 'logspace', 'params': {'start': .09, 'stop': .05, 'num': k}}
    genalger.p_mutation_dict = {'method': 'linspace', 'params': {'start': .5, 'stop': .2, 'num': k}}
    genalger.n_mutation_dict ={'method': 'repeat', 'params1': {'start': 0, 'end': k//2, 'value': 20, 'repetitions': k//2},
                      'params2': {'start': k//2+1, 'end': k, 'value': 10, 'repetitions': k//2}}
    genalger.p_crossing_over_dict = {'method': 'linspace', 'params': {'start': .6, 'stop': .5, 'num': k}}

    genalger.ev_param = np.linspace(**genalger.ev_param_dict['params'])
    genalger.penalty_param = np.logspace(**genalger.penalty_param_dict['params'])
    genalger.p_mutation = np.linspace(**genalger.p_mutation_dict['params'])
    genalger.n_mutations = np.repeat(genalger.n_mutation_dict['params1']['value'], k)
    genalger.n_mutations[k//2:] = genalger.n_mutation_dict['params2']['value']
    genalger.p_crossing_over = np.linspace(**genalger.p_crossing_over_dict['params'])

    
    genalger.genCycle(k)
    #genalger.summary()
    saveToFile(genalger)


def param2():
    data_pd, group_indicator = aux.loadData(filenumber=0)
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    
    NF = NormFinder(data_pd.values, group_indicator)
    
    genalger = GenAlger(data_pd, null_stats=null_stats, estimator=NF,
                        population_size=200, init_ones=0.2)
    
    k = 100 # number of generations
    genalger.ev_param = np.linspace(2.5, 3., num=k)
    genalger.penalty_param = np.linspace(0.2, 0.9, num=k)
    genalger.p_mutation = np.linspace(.5, .2, num=k)
    genalger.n_mutations = genalger.population_size//8
    genalger.p_crossing_over = .5 
    
    genalger.genCycle(k)
    genalger.summary()


def param3():
    data_pd, group_indicator = aux.loadData(filenumber=0)
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    
    GN = GeNorm(data_pd.values, synth_method='geometric')
    
    genalger = GenAlger(data_pd, null_stats=null_stats, estimator=GN,
                        population_size=200, init_ones=0.2)
    
    k = 100 # number of generations
    genalger.genCycle(k)
    genalger.summary()



plt.close('all')
tstart = time()
param1()
print("\nTotal time: %.2f s" %(time()-tstart))