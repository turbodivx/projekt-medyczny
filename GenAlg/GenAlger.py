# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import time

import auxiliaryFunctions as aux
import evolutionaryAlgorithm.evolutionaryAlgorithmFunctions as eaf
import evolutionaryAlgorithm.evolutionaryAlgorithmSelections as eas
import plots
from evaluationMethods.NormFinder import NormFinder
from evaluationMethods.GeNorm import GeNorm
from evaluationMethods.BestKeeper import BestKeeper


class GenAlger:
    def __init__(self, population_size=200, estimator=None):

        self.population_size = population_size
        self.estimator = estimator


    def loadData(self, data_pd, null_stats=None, init_ones=0.1):

        self.mirnames = data_pd.index
        self.chromosome_length = data_pd.shape[0]

        if null_stats is not None:
            self.null_stats = null_stats
        else:
            null_stats = np.zeros(self.chromosome_length)

        # initial chromosome sum (=no. of ones)
        if ((type(init_ones) == int) & (1 < init_ones < self.chromosome_length)):
            self.init_ones = init_ones
        elif ((type(init_ones) == float) & (0 < init_ones < 1)):
            self.init_ones = int(init_ones * self.chromosome_length)
        else:
            self.init_ones = int(0.1 * self.chromosome_length)

        eaf.getZeroGeneration(self)
        self.generation_number = 0
        self.getZeroGenerationParams()


    def getZeroGenerationParams(self):
        self.all_Ms = np.empty((self.population_size,0))
        self.chromosomes_accumulation = np.empty((self.chromosome_length,0))
        self.best_chrom = np.empty((self.chromosome_length,0))
        self.getMs()
        
        # setting defaults for the genetic cycles, can be changed externally
        self.p_mutation = 0.3
        self.n_mutations = 20
        self.p_crossing_over = 0.5
        self.xo_type = 'decoupled'
        self.ev_type = 'power'
        self.ev_param = 3.
        self.penalty_type = 'U-shape'
        self.penalty_param = .07
        self.penalty_target = 3.5
        self.null_stats_contribution = 0.1
        self.selection_method = {'method': 'RW', 'param1': 1, 'param2': 4}
        
    
    def typechecker(self, varname, k):
        var = getattr(self, varname)
        if (type(var) in [int, float]):
            setattr(self, varname, np.repeat(var, k))
            
        
    def genCycle(self, number_of_generations=150):
        for attrname in ['p_mutation', 'n_mutations', 'p_crossing_over',
                         'ev_param', 'penalty_param']:
            self.typechecker(attrname, number_of_generations)
        
        plots.chromosomesSubplots(self, 121, title="Original chromosomes")
        
        print ("Genetic algorithm cycles:")
        for g in tqdm(range(number_of_generations)):
            
            evaluation = eaf.evaluateGeneration(self) # evaluate the generation
            eas.getNewGeneration(self, evaluation)
            eaf.mutation(self)
            eaf.crossing_over(self)
            self.getMs() # Ms for the new generations

            
    def getMs(self):
        # list of M values for current chromosomes
        M_collection = []
        for col in range(self.chromosomes.shape[1]):
            if sum(self.chromosomes[:,col]) == 0:
                self.chromosomes[np.random.randint(self.chromosomes.shape[0]), col] = 1
                
            M_collection.append(self.estimator.get_stability(chromosome=self.chromosomes[:,col]))
            
        self.current_Ms = M_collection
        self.all_Ms = np.append(self.all_Ms, np.array(self.current_Ms).reshape(-1,1), axis=1)
        self.chromosomes_accumulation = np.append(self.chromosomes_accumulation,
                                             np.sum(self.chromosomes, axis=1).reshape(-1,1), axis=1)
        self.best_chrom = np.append(self.best_chrom,
                                    self.chromosomes[:,np.nanargmin(self.current_Ms)].reshape(-1,1), axis=1)
            
    
    def summary(self, showPlots=True):
        print("\nBest M: %.4f" %np.nanmin(self.current_Ms))
        best_mirs_idx = np.where(self.chromosomes[:,np.nanargmin(self.current_Ms)]==1)[0]
        best_mirs = (self.mirnames[best_mirs_idx]).values
        print('%d best miRs: %s (indices: %s)' %(len(best_mirs), best_mirs, best_mirs_idx))
        
        if showPlots:
            # summary - plots
            plots.chromosomesSubplots(self, 122, title="Final chromosomes")
            plots.genAlgCycles(self)
            plots.chromAccum(self.chromosomes_accumulation)
            plots.chromAccum(self.best_chrom, title='Best chromosome over generations')
            plots.paramsPlots(self)
            
            
#=-----------------------------------------------------------------------------
if __name__ == '__main__':
    plt.close('all')
    tstart = time.time()
    data_pd, group_indicator = aux.loadData(filenumber=0, whereFrom='here') # load the data (to pandas DF)
    data_pd, null_stats = aux.optimisedDropNa(data_pd, k=0.75, verbose=False)
    
    # estimators
    NF = NormFinder(data_pd.values, group_indicator)
    GN = GeNorm(data_pd.values)
    BK = BestKeeper(data_pd.values, corr_method='pearsonr')
    
    genalger = GenAlger( estimator=GN)
    genalger.loadData(data_pd, null_stats=null_stats)
    genalger.genCycle(10) # default parameters
    genalger.summary()
    print("\nTotal time: %.2f s" %(time.time()-tstart))