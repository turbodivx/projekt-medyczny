import unittest

import numpy as np
import pandas as pd

from genorm import GeNormGenerator


class TestGeNorm(unittest.TestCase):

    test_filename = 'test.xlsx'

    def setUp(self):
        self.normalizedGene = GeNormGenerator()
        self.test_filename = 'test.xlsx'
        self.df = pd.read_excel(self.test_filename, index_col=0)
        self.data = self.normalizedGene.prepare_data(self.df)

    def test_prepared_matrix_shape(self):
        self.assertEqual(self.data.shape, (3, 3), 'Shape is not ok.')

    def test_fill_matrix(self):
        prepared_matrix = self.normalizedGene.prepare_matrix(self.data)
        matrix = self.normalizedGene.fill_matrix(prepared_matrix, self.data.values)
        first = matrix[0, 1]
        second = np.array([-1, -1, 0], dtype=np.float)
        self.assertTrue(np.array_equal(first, second))

    def test_fill_std_matrix(self):
        prepared_matrix = self.normalizedGene.prepare_matrix(self.data)
        matrix = self.normalizedGene.fill_matrix(prepared_matrix, self.data.values)
        std_matrix = self.normalizedGene.fill_std_matrix(matrix)
        self.assertAlmostEqual(std_matrix[0, 1], 0.47140452079103, places=13)
